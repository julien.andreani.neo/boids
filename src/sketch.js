let flock;
let fpsDiv;
let headingChkbox;
let rangeChkbox;
let quadtreeChkbox;

function setup() {
    createCanvas(800, 600);

    fpsDiv = createDiv();
    fpsDiv.style("font-size", "30px");
    headingChkbox = createCheckbox("Show heading", false);
    rangeChkbox = createCheckbox("Show range", false);
    quadtreeChkbox = createCheckbox("Show quadtree", false);

    flock = new Flock(new Box(0, 0, width, height));
    createRandomBoids(20);
}

function draw() {
    background(80);
    flock.run(headingChkbox.checked(), rangeChkbox.checked());
    if (quadtreeChkbox.checked())
        flock.showBorders();

    fpsDiv.html(Math.round(frameRate()));

    // if (flock.qt.points.length >= 2)
    //     noLoop();
}

function mouseDragged() {
    flock.addBoid(mouseX, mouseY);
}

function mouseClicked() {
    flock.addBoid(mouseX, mouseY);
}

function createRandomBoids(maxBoids) {
    for (let i = 0; i < maxBoids; ++i)
        flock.addBoid(random(width), random(height));
}

// TESTS ============================================================

// let pos, radius, heading, angle, container;
// let p = []

// function setup() {
//     createCanvas(800, 600);
//     pos = createVector(width / 2, height / 2);
//     radius = 100;
//     heading = -PI / 2;
//     angle = PI * 3 / 2;
//     container = new Arc(pos.x, pos.y, radius, heading, angle);
// }

// function draw() {
//     background(80);

//     drawSystem(pos, radius, heading);

//     let hvec = createVector(cos(heading), sin(heading));
//     hvec.mult(radius);
//     drawArrow(pos, hvec, "cyan");

//     for (const pt of p)
//         drawPoint(pt, container.contains(pt) ? "lime" : "red");
// }

// function mouseDragged() {
//     p.push(createVector(mouseX, mouseY));
// }

// function drawSystem(pos, radius, heading) {
//     drawPoint(pos, "white");
//     noFill();
//     stroke(255, 100);
//     strokeWeight(1);
//     arc(pos.x, pos.y, radius * 2, radius * 2, heading - PI * 3 / 4, heading + PI * 3 / 4, PIE);
// }

// function drawPoint(pos, col = "black") {
//     strokeWeight(4);
//     stroke(col);
//     point(pos.x, pos.y);
// }

// function drawArrow(base, vec, col = "black") {
//     push();
//     stroke(col);
//     strokeWeight(1);
//     fill(col);
//     translate(base.x, base.y);
//     line(0, 0, vec.x, vec.y);
//     rotate(vec.heading());
//     let arrowSize = 7;
//     translate(vec.mag() - arrowSize, 0);
//     triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
//     pop();
// }