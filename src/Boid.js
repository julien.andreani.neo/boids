class Boid {
    constructor(_x = random(width), _y = random(height)) {
        this.maxForce = 0.05;
        this.maxSpeed = 3;
        this.fov = PI * 8 / 6;
        // this.fov = TWO_PI;
        this.fovRange = 100;

        this.pos = createVector(_x, _y);
        this.vel = p5.Vector.random2D();
        this.vel.setMag(this.maxSpeed);
        this.acc = createVector();

        this.color = color(75, 148, 242);
        this.strokeColor = color(41, 0, 230);
        this.frontDelta = 10;
        this.backDelta = this.frontDelta * 2 / 3;
        this.desiredSep = this.frontDelta * 2;
        this.desiredAli = this.fovRange;
        this.desiredCoh = this.fovRange;
        this.desiredBound = this.frontDelta * 5;
    }

    separation(neighborhood) {
        let steer = createVector(0, 0);
        if (neighborhood.length > 1) {
            let count = 0;
            for (const neighbor of neighborhood) {
                let d = p5.Vector.dist(this.pos, neighbor.data.pos);
                if (neighbor.data !== this && d < this.desiredSep) {
                    let diff = p5.Vector.sub(this.pos, neighbor.data.pos);
                    // diff.normalize();
                    diff.div(d);
                    steer.add(diff);
                    count++;
                }
            };
            if (count > 0)
                steer.div(count);
            if (steer.mag() > 0) {
                steer.setMag(this.maxSpeed);
                steer.sub(this.vel);
                steer.limit(this.maxForce);
            }
        }
        return steer;
    }

    alignment(neighborhood) {
        let steer = createVector(0, 0);
        if (neighborhood.length > 0) {
            let count = 0;
            for (const neighbor of neighborhood) {
                let d = p5.Vector.dist(this.pos, neighbor.data.pos);
                if (neighbor.data !== this && d < this.desiredAli) {
                    steer.add(neighbor.data.vel);
                    count++;
                }
            };
            if (count > 0) {
                steer.div(count);
                steer.setMag(this.maxSpeed);
                steer.sub(this.vel);
                steer.limit(this.maxForce);
            }
        }
        return steer;
    }

    cohesion(neighborhood) {
        let steer = createVector(0, 0);
        if (neighborhood.length > 0) {
            let count = 0;
            for (const neighbor of neighborhood) {
                let d = p5.Vector.dist(this.pos, neighbor.data.pos);
                if (neighbor.data !== this && d < this.desiredCoh) {
                    steer.add(neighbor.data.pos);
                    count++;
                }
            };
            if (count > 0) {
                steer.div(count);
                return this.seek(steer);
            }
        }
        return steer;
    }

    boundaries(container) {
        let steer = createVector(0, 0);
        if (this.pos.x < container.x + this.desiredBound)
            steer.x = 10;
        else if (this.pos.x > container.x + container.w - this.desiredBound)
            steer.x = -10;
        if (this.pos.y < container.y + this.desiredBound)
            steer.y = 10;
        else if (this.pos.y > container.y + container.h - this.desiredBound)
            steer.y = -10;
        steer.limit(this.maxForce);
        return steer;
    }

    seek(target) {
        // With arrival behaviour
        // let targetOffset = p5.Vector.sub(target, this.pos);
        // const d = targetOffset.mag();
        // const rampedSpeed = this.maxSpeed * (d / (this.fovRange * 0.5)); // slowing distance = 20% of fovRange
        // const clippedSpeed = rampedSpeed <= this.maxSpeed ? rampedSpeed : this.maxSpeed;
        // const desired = targetOffset.mult(clippedSpeed / d);
        // const steer = p5.Vector.sub(desired, this.vel);
        // steer.limit(this.maxForce);
        // this.acc.add(steer);

        // Without arrival behaviour
        let desired = p5.Vector.sub(target, this.pos);
        desired.setMag(this.maxSpeed);
        let steer = p5.Vector.sub(desired, this.vel);
        steer.limit(this.maxForce);

        return steer;
    }

    applyForce(force) {
        this.acc.add(force);
    }

    update() {
        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);
        this.pos.add(this.vel);
        this.acc.mult(0);

        // const edge = 0;
        // if (this.pos.x < -edge) this.pos.x = width + edge;
        // else if (this.pos.x > width + edge) this.pos.x = -edge;
        // if (this.pos.y < -edge) this.pos.y = height + edge;
        // else if (this.pos.y > height + edge) this.pos.y = -edge;
    }

    render(showHeading = false, showFovRange = false) {
        push();
        fill(this.color);
        stroke(this.strokeColor);
        strokeWeight(1);

        const frontD = this.frontDelta;
        const backD = this.backDelta;
        const frontVector = this.vel.heading();
        const leftVector = this.vel.heading() - PI * 3 / 4;
        const rightVector = this.vel.heading() + PI * 3 / 4;

        const x1 = this.pos.x + cos(frontVector) * frontD;
        const y1 = this.pos.y + sin(frontVector) * frontD;
        const x2 = this.pos.x + cos(leftVector) * backD;
        const y2 = this.pos.y + sin(leftVector) * backD;
        const x3 = this.pos.x + cos(rightVector) * backD;
        const y3 = this.pos.y + sin(rightVector) * backD;
        beginShape();
        vertex(x1, y1);
        vertex(x2, y2);
        vertex(x3, y3);
        endShape(CLOSE);

        if (showHeading) {
            let base = createVector(x1, y1);
            let p2 = createVector(this.pos.x + cos(frontVector) * frontD * 3, this.pos.y + sin(frontVector) * frontD * 3);
            let v = p5.Vector.sub(p2, base);
            v.setMag(this.vel.mag()).mult(20);
            line(base.x, base.y, v.x + this.pos.x, v.y + this.pos.y);
        }

        if (showFovRange) {
            noFill();
            strokeWeight(1);
            stroke(255, 75);

            // Fov
            const heading = this.vel.heading();
            const halfRange = this.fov / 2;
            let r2 = this.fovRange * 2;
            arc(this.pos.x, this.pos.y, r2, r2, heading - halfRange, heading + halfRange, PIE);

            // Separation range
            stroke(0, 255, 0, 75);
            r2 = this.desiredSep * 2;
            arc(this.pos.x, this.pos.y, r2, r2, heading - halfRange, heading + halfRange, PIE);

            // Alignment range
            stroke(255, 0, 0, 75);
            r2 = this.desiredAli * 2;
            arc(this.pos.x, this.pos.y, r2, r2, heading - halfRange, heading + halfRange, PIE);
        }
        pop();
    }
}