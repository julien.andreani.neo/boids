class Vehicle {
    constructor(_x, _y) {
        this.position = createVector(_x, _y);
        this.velocity = createVector(0, 0);
        this.acceleration = createVector(0, 0);
        this.r = 12;
        this.maxspeed = 3;
        this.maxforce = 0.2;
    }

    applyForce(force) {
        // We could add mass here if we want A = F / M
        this.acceleration.add(force);
    }

    applyBehaviors(vehicles) {
        let separateForce = this.separate(vehicles);
        let seekForce = this.seek(createVector(mouseX, mouseY));
        separateForce.mult(2);
        seekForce.mult(1);
        this.applyForce(separateForce);
        this.applyForce(seekForce);
    }

    // A method that calculates a steering force towards a target
    // STEER = DESIRED MINUS VELOCITY
    seek(target) {
        let desired = p5.Vector.sub(target, this.position);  // A vector pointing from the position to the target

        // Normalize desired and scale to maximum speed
        desired.normalize();
        desired.mult(this.maxspeed);
        // Steering = Desired minus velocity
        let steer = p5.Vector.sub(desired, this.velocity);
        steer.limit(this.maxforce);  // Limit to maximum steering force

        return steer;
    }

    // Separation
    // Method checks for nearby vehicles and steers away
    separate(vehicles) {
        let desiredseparation = this.r * 2;
        let sum = createVector();
        let count = 0;
        // For every boid in the system, check if it's too close
        vehicles.forEach(other => {
            let d = p5.Vector.dist(this.position, other.position);
            // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
            if ((d > 0) && (d < desiredseparation)) {
                // Calculate vector pointing away from neighbor
                let diff = p5.Vector.sub(this.position, other.position);
                diff.normalize();
                diff.div(d);        // Weight by distance
                sum.add(diff);
                count++;            // Keep track of how many
            }
        });
        // Average -- divide by how many
        if (count > 0) {
            sum.div(count);
            // Our desired vector is the average scaled to maximum speed
            sum.normalize();
            sum.mult(this.maxspeed);
            // Implement Reynolds: Steering = Desired - Velocity
            sum.sub(this.velocity);
            sum.limit(this.maxforce);
        }
        return sum;
    }


    // Method to update position
    update() {
        // Update velocity
        this.velocity.add(this.acceleration);
        // Limit speed
        this.velocity.limit(this.maxspeed);
        this.position.add(this.velocity);
        // Reset accelertion to 0 each cycle
        this.acceleration.mult(0);
    }

    display() {
        fill(175);
        stroke(0);
        push();
        translate(this.position.x, this.position.y);
        ellipse(0, 0, this.r);
        pop();
    }
}

// A list of vehicles
let vehicles = [];

function setup() {
    createCanvas(640, 360);
    // We are now making random vehicles and storing them in an ArrayList
    for (let i = 0; i < 100; i++) {
        vehicles.push(new Vehicle(random(width), random(height)));
    }
}

function draw() {
    background(255);

    vehicles.forEach(v => {
        // Path following and separation are worked on in this function
        v.applyBehaviors(vehicles);
        // Call the generic run method (update, borders, display, etc.)
        v.update();
        v.display();
    });

    // Instructions
    fill(0);
    text("Drag the mouse to generate new vehicles.", 10, height - 16);
}


function mouseDragged() {
    vehicles.push(new Vehicle(mouseX, mouseY));
}
