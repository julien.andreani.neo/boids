class Flock {
    constructor(_container, _points = [], _capacity = 4) {
        this.container = _container;
        this.capacity = _capacity;
        this.qt = new Quadtree(this.container, _points, this.capacity);
    }

    addBoid(x, y) {
        this.qt.insert(new Point(x, y, new Boid(x, y)));
    }

    run(showHeading = false, showFovRange = false) {
        let points = this.qt.getAllPoints();
        for (let point of points) {
            const query = this.qt.query(new Arc(point.x, point.y, point.data.fovRange, point.data.vel.heading(), point.data.fov));

            let sepForce = point.data.separation(query);
            let aliForce = point.data.alignment(query);
            let cohForce = point.data.cohesion(query);
            let boundForce = point.data.boundaries(this.container);

            sepForce.mult(2);
            aliForce.mult(1);
            cohForce.mult(1);
            boundForce.mult(0.2);

            point.data.applyForce(sepForce);
            point.data.applyForce(aliForce);
            point.data.applyForce(cohForce);
            point.data.applyForce(boundForce);

            // let seekForce = point.data.seek(createVector(mouseX, mouseY));
            // seekForce.mult(1);
            // point.data.applyForce(seekForce);
        }
        for (let point of points) {
            point.data.update();
            point.x = point.data.pos.x;
            point.y = point.data.pos.y;
            point.data.render(showHeading, showFovRange);
        }
        this.qt.clear();
        this.qt = new Quadtree(this.container, points, this.capacity);
    }

    showBorders() {
        this.showBordersRecursive(this.qt);
    }

    showBordersRecursive(node) {
        if (!node)
            return;
        push();
        stroke(255);
        strokeWeight(1);
        const x = node.container.x;
        const y = node.container.y;
        const w = node.container.w;
        const h = node.container.h;
        line(x, y, x + w, y);
        line(x + w, y, x + w, y + h);
        line(x + w, y + h, x, y + h);
        line(x, y + h, x, y);
        pop();
        this.showBordersRecursive(node.ne);
        this.showBordersRecursive(node.nw);
        this.showBordersRecursive(node.se);
        this.showBordersRecursive(node.sw);
    }
}