class Arc extends Circle {
    constructor(_x, _y, _r, _heading, _angle, _data) {
        super(_x, _y, _r, _data)
        this.angle = _angle;
        this.heading = _heading;
    }
    magSq(v) {
        return v.x * v.x + v.y * v.y;
    }

    mag(v) {
        return Math.sqrt(this.magSq(v));
    }

    dot(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y;
    }

    angleBetween(v1, v2) {
        const dotmagmag = this.dot(v1, v2) / (this.mag(v1) * this.mag(v2));
        let angle;
        angle = Math.acos(this.min(1, this.max(-1, dotmagmag)));
        return angle;
    }

    contains(point) {
        const v1 = {
            x: Math.cos(this.heading),
            y: Math.sin(this.heading)
        };
        const v2 = {
            x: point.x - this.x,
            y: point.y - this.y
        };
        return this.euclideanDistance(point, this) <= this.rSq && this.angleBetween(v1, v2) <= this.angle / 2;
    }
}