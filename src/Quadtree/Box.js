class Box {
    constructor(_x, _y, _w, _h, _data) {
        this.x = _x;
        this.y = _y;
        this.w = _w;
        this.h = _h;
        this.data = _data;
    }

    contains(point) {
        return point.x >= this.x && point.x <= this.x + this.w && point.y >= this.y && point.y <= this.y + this.h;
    }

    intersects(box) {
        return !(box.x > this.x + this.w || box.x + box.w < this.x || box.y > this.y + this.h || box.y + box.h < this.y);
    }
}