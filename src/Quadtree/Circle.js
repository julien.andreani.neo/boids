class Circle {
    constructor(_x, _y, _r, _data) {
        this.x = _x;
        this.y = _y;
        this.r = _r;
        this.rSq = this.r * this.r;
        this.data = _data;
    }

    euclideanDistance(p1, p2) {
        return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
    }

    max(a, b) {
        return a >= b ? a : b;
    }

    min(a, b) {
        return a <= b ? a : b;
    }

    contains(point) {
        return this.euclideanDistance(point, this) <= this.rSq;
    }

    intersects(box) {
        const dx = this.x - this.max(box.x, this.min(this.x, box.x + box.w));
        const dy = this.y - this.max(box.y, this.min(this.y, box.y + box.h));
        return dx * dx + dy * dy <= this.rSq;
    }
}