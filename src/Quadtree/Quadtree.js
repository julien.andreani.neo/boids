class Quadtree {
    constructor(_container, _points = [], _capacity = 4) {
        this.capacity = _capacity;
        this.container = _container;
        this.isDivided = false;
        this.points = [];
        this.ne = undefined;
        this.nw = undefined;
        this.se = undefined;
        this.sw = undefined;

        for (const point of _points)
            this.insertRecursive(point);
    }

    arePointsEqual(p1, p2) {
        return p1.x === p2.x && p1.y === p2.y;
    }

    getTree() {
        let tree;

        if (this.isDivided)
            tree = {
                ne: this.ne.getTree(),
                nw: this.nw.getTree(),
                se: this.se.getTree(),
                sw: this.sw.getTree()
            };
        else
            tree = this.getNodePointAmount();
        return tree;
    }

    getAllPoints() {
        const pointList = [];
        this.getAllPointsRecursive(pointList);
        return pointList;
    }

    getAllPointsRecursive(pointList) {
        if (!this.isDivided) {
            Array.prototype.push.apply(pointList, this.points.slice());
            return;
        }

        this.ne.getAllPointsRecursive(pointList);
        this.nw.getAllPointsRecursive(pointList);
        this.se.getAllPointsRecursive(pointList);
        this.sw.getAllPointsRecursive(pointList);
    }

    getNodePointAmount() {
        return this.points.length;
    }

    divide() {
        this.isDivided = true;

        const x = this.container.x;
        const y = this.container.y;
        const w = this.container.w / 2;
        const h = this.container.h / 2;

        this.ne = new Quadtree(new Box(x + w, y, w, h), this.points.slice());
        this.nw = new Quadtree(new Box(x, y, w, h), this.points.slice());
        this.se = new Quadtree(new Box(x + w, y + h, w, h), this.points.slice());
        this.sw = new Quadtree(new Box(x, y + h, w, h), this.points.slice());

        this.points.length = 0;
        delete this.points;
        this.points = [];
    }

    insert(pointOrArray) {
        if (Array.isArray(pointOrArray))
            for (const point of pointOrArray)
                this.insertRecursive(point)
        else
            this.insertRecursive(pointOrArray);

    }

    insertRecursive(point) {
        if (!this.container.contains(point))
            return false;
        if (!this.isDivided) {
            if (this.getNodePointAmount() < this.capacity) {
                this.points.push(point);
                return true;
            } else
                this.divide();
        }
        if (this.ne.insertRecursive(point)) return true;
        if (this.nw.insertRecursive(point)) return true;
        if (this.se.insertRecursive(point)) return true;
        if (this.sw.insertRecursive(point)) return true;
        return false;
    }

    remove(pointOrArray) {
        if (Array.isArray(pointOrArray))
            for (const point of pointOrArray)
                this.removeRecursive(point)
        else
            this.removeRecursive(pointOrArray);

    }

    removeRecursive(point) {
        if (!this.container.contains(point))
            return;
        if (!this.isDivided) {
            const len = this.points.length;
            for (let i = len - 1; i >= 0; --i)
                if (this.arePointsEqual(point, this.points[i]))
                    this.points.splice(i, 1);
            return;
        }

        this.ne.removeRecursive(point);
        this.nw.removeRecursive(point);
        this.se.removeRecursive(point);
        this.sw.removeRecursive(point);

        if (this.ne.getNodePointAmount() === 0 && !this.ne.isDivided() &&
            this.nw.getNodePointAmount() === 0 && !this.nw.isDivided() &&
            this.se.getNodePointAmount() === 0 && !this.se.isDivided() &&
            this.sw.getNodePointAmount() === 0 && !this.sw.isDivided()) {
            this.isDivided = false;
            delete this.ne;
            delete this.nw;
            delete this.se;
            delete this.sw;
        }
    }

    query(range) {
        const pointsFound = [];
        this.queryRecursive(range, pointsFound);
        return pointsFound;
    }

    queryRecursive(range, pointsFound) {
        if (range.intersects(this.container)) {
            if (this.isDivided) {
                this.ne.queryRecursive(range, pointsFound);
                this.nw.queryRecursive(range, pointsFound);
                this.se.queryRecursive(range, pointsFound);
                this.sw.queryRecursive(range, pointsFound);
            } else {
                const p = this.points.filter(point => range.contains(point));
                Array.prototype.push.apply(pointsFound, p);
            }
        }
    }

    clear() {
        this.points = [];
        this.isDivided = false;
        delete this.ne;
        delete this.nw;
        delete this.se;
        delete this.sw;
    }
}